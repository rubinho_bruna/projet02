
import java.io.IOException;
import java.util.Date;
import utfpr.ct.dainf.if62c.projeto.Agenda;
import utfpr.ct.dainf.if62c.projeto.Compromisso;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class Projeto2 {
 
    public static void main(String[] args) throws IOException {
        //Cria um novo Timer que não executa como um daemon. 
        //Atribui o nome name ao subprocesso associado.
        Agenda agenda = new Agenda("Minha Agenda");
        //System.currentTimeMillis();
        //A diferença, medida em milissegundos, entre a hora atual e a 
        //meia-noite do dia 1 de janeiro de 1970 (UTC).
        Date data1 = new Date(System.currentTimeMillis() + 20000); // + 20 segundos
        // Atualmente existem apenas dois construtores válidos, Date() e Date(milissegundos).
        //Cria um objeto do tipo Compromisso
        Compromisso c1 = new Compromisso(data1, "Compromisso 1");
        agenda.novoCompromisso(c1); //Cria um novoCompromisso
        //A antecedência é dada em segundos. Este método deve criar um aviso 
        //para o compromisso, registrá-lo na lista de avisos do compromisso e 
        //agendar a execução do aviso para ocorrer com a antecedência 
        //especificada em relação à data do compromisso.
        agenda.novoAviso(c1, 11); //Cria um novoAviso 
        agenda.novoAviso(c1, 9);
        //A antecedência e o intervalo são dados em segundos. Este método deve 
        //criar um aviso para o compromisso, registrá-lo na lista de avisos do 
        //compromisso e agendar a execução do aviso para ocorrer a primeira vez 
        //com a antecedência especificada em relação à data do compromisso e 
        //depois a cada intervalo especificado.
        agenda.novoAviso(c1, 18, 2);

        Date data2 = new Date(System.currentTimeMillis() + 15000); // + 15 segundos
        Compromisso c2 = new Compromisso(data2, "Compromisso 2");
        agenda.novoCompromisso(c2);
        agenda.novoAviso(c2, 11);
        agenda.novoAviso(c2, 7);
        agenda.novoAviso(c2, 6, 1);
     
        System.out.println("Pressione Enter para terminar...");
        System.in.read();
        agenda.destroi();
    }
 
}